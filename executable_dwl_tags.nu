#!/usr/bin/env nu

def main [] {
  let fname = $"($env.HOME)/.cache/dwltags";
  let monitors = (wlr-randr | lines | where (($it starts-with eDP-1) or ($it starts-with HDMI-A-1) or ($it starts-with DP-2)) | each {|it| $it | split row " " | $in.0})
  # print $monitors
  loop {
  inotifywait -qq --event modify $fname
  # Get info from the file
  let output = (tail -n21 $fname);
  # print $output;
  $monitors | each {
    |monitor| 
      
      mut title = ($output | grep $"($monitor) title" | tail -1 | cut -d ' ' -f 3- | str substring 0..60)
      let title_length = ($title | str length)
      if $title_length < 1 {
        $title = "...";
      } 
      let selmon = ($output | grep 'selmon 1' | tail -1 | cut -d ' ' -f 1 )
      let layout = ($output | grep $"($monitor) layout" | tail -1 | cut -d ' ' -f 3- )
      # # Get the tag bit mask as a decimal
      let activetags = ($output | grep $"($monitor) tags" | tail -1 | awk '{print $3}' | into int)
      let selectedtags = (echo $output | grep $"($monitor) tags" | tail -1 | awk '{print $4}' | into int)
      return (cycle $selectedtags $activetags $title $selmon $layout)
  } | reverse | to json --raw | print 
  }
}

def cycle [selectedtags: int, activetags: int, title: string, selmon: string, layout: string] {
  let tags = ["1" "2" "3" "4" "5" "6" "7" "8" "9"]
  mut dwl_mon_state = {title: $"($title)", mon_active: false, tags: []}
  $dwl_mon_state.mon_active = $selmon;
  $dwl_mon_state."tags" = ($tags | each {
    |x| 
      let it = ($x | into int)
      mut t_record = {id: $it, focused: false, occupied: false}
      let mask = 1 bit-shl ($it - 1)
      if ($selectedtags bit-and $mask) > 0 {
        $t_record.focused = true;
      } 
      if ($activetags bit-and $mask) > 0 {
        $t_record.occupied = true;
      }
      $t_record;
  } | filter {|f| $f.occupied == true or $f.focused == true});
  return $dwl_mon_state;
}
