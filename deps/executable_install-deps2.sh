curl https://sh.rustup.rs -sSf | sh

git clone https://github.com/elkowar/eww
cd eww
cargo build --release --no-default-features --features=wayland

curl -fsSL https://fnm.vercel.app/install | bash
LV_BRANCH='release-1.2/neovim-0.8' bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/fc6873809934917b470bff1b072171879899a36b/utils/installer/install.sh)

curl -fsSL https://deno.land/x/install/install.sh | sh
