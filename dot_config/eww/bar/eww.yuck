;; Variables
(defpoll clock_time :interval "5m" "date +\%I")
(defpoll clock_minute :interval "5s" "date +\%M")
(defpoll clock_date :interval "10h" "date '+%d/%m'")
(defpoll volume_percent :interval "3s" "amixer -D pulse sget Master | grep 'Left:' | awk -F'[][]' '{ print $2 }' | tr -d '%'")
(defpoll mic_percent :interval "3s" "amixer -D pulse sget Capture | grep 'Left:' | awk -F'[][]' '{ print $2 }' | tr -d '%'")
(defpoll brightness_percent :interval "5s" "brightnessctl -m -d intel_backlight | awk -F, '{print substr($4, 0, length($4)-1)}' | tr -d '%'")
(defpoll battery :interval "15s" "./scripts/battery --bat")
(defpoll battery_status :interval "1m" "./scripts/battery --bat-st")
(defpoll memory :interval "15s" "scripts/memory")
(defpoll memory_used_mb :interval "2m" "scripts/mem-ad used")
(defpoll memory_total_mb :interval "2m" "scripts/mem-ad total")
(defpoll memory_free_mb :interval "2m" "scripts/mem-ad free")
(defvar vol_reveal false)
(defvar show false)
(defvar br_reveal false)
(defvar notif_reveal true)
(defvar music_reveal false)
(defvar wifi_rev false)
(defvar time_rev false)
(deflisten all_workspaces :initial "[{\"id\":\"1\", \"windows\": \"2\"}]" "~/dwl_tags.nu")


(defvar eww "eww -c $HOME/.config/eww/bar")


(defpoll COL_WLAN :interval "1m" "~/.config/eww/bar/scripts/wifi --COL")
(defpoll ESSID_WLAN :interval "1m" "~/.config/eww/bar/scripts/wifi --ESSID")
(defpoll WLAN_ICON :interval "1m" "~/.config/eww/bar/scripts/wifi --ICON")


(defpoll calendar_day :interval "20h" "date '+%d'")
(defpoll calendar_year :interval "20h" "date '+%Y'")

;; widgets

(defwidget wifi []
  (eventbox :onhover "${eww} update wifi_rev=true"
    :onhoverlost "${eww} update wifi_rev=false"
    (box :space-evenly "false" :halign "center"
      :style "padding: 6px; padding-top: 4px; margin-right: 6px"
      :valign "center"
      :orientation "v"
      (button :class "module-wif" :onclick "networkmanager_dmenu" :style "color: ${COL_WLAN};" "")
        (revealer :transition "slidedown"
        :reveal wifi_rev
        :tooltip "Hello!"
        :duration "350ms"

      (expander
        :expanded wifi_rev
        (label    :class "module_essid"
          :text {ESSID_WLAN}
          :orientation "h"
        ))
          )
      )))


(defwidget bat []
  (box :class "bat_module" :vexpand "false" :hexpand "false"
    (circular-progress :value battery
      :class "batbar"
      :thickness 4
      (button
        :class "iconbat"
        :limit-width 2
        :tooltip "battery on ${battery}%"
        :show_truncated false
        :onclick "$HOME/.config/eww/bar/scripts/pop system"
        :wrap false
      ""))))


(defwidget workspaces [monitor]
  (eventbox
    (box :vexpand "false" :class "iconbat" :space-evenly true :orientation "h"
      ; :style "background-color: #ffffff"
      (for workspace in {all_workspaces[monitor].tags}
          (box :class "workspace_icon ${workspace.focused ? "workspace_enabled": "workspace_disabled"}"
            (label :text "${workspace.id}")
          )
      )
    )
  )
)

(defwidget mem []
  (box :class "mem_module" :vexpand "false" :hexpand "false"
    (circular-progress :value memory
      :class "membar"
      :thickness 4
      (button
        :class "iconmem"
        :limit-width 2
        :tooltip "using ${memory}% ram"
        :onclick "$HOME/.config/eww/bar/scripts/pop system"
        :show_truncated false
        :wrap false
      ""))))


(defvar mons "[\"eDP-1\", \"HDMI-A-1\", \"DP-2\"]")

(defwidget monitors []
  (box
    :orientation "h"
    :halign "end"
    :valign "center"
    :style "border-radius: 15px; background-color: rgba(0,0,0,0.25);"
    :hexpand false
    (label :class "monitor_current" :text "${all_workspaces[0].mon_active}")
  ))

(defwidget window_title [monitor]
  (box 
    :style "border-radius: 15px; background-color: rgba(0,0,0,0.65); padding: 2px; margin-right: 2px;"

    (label :class "win_title" :text "${all_workspaces[monitor].title}")))

    ; (label :class "win_title" :text "...")))

(defwidget right [monitor]
  (box 
    :style "border-radius: 15px; background-color: rgba(0,0,0,1);"
    :space-evenly false
    :halign "end"
    (monitors)
    (volume)
    (wifi)
    (clock_module)
    ))
(defwidget center [monitor]
  (box :orientation "h"
    :style "margin-left: 24px"
    :hexpand "false"
    :halign "center"
    :valign "center"
    (window_title :monitor monitor)))



(defwidget top_left [monitor]
  (box :orientation "h"
    :class "left_modules"
    :space-evenly false
    :halign "start"
    :hexpand "false"
    :style "border-radius: 15px; background-color: rgba(0,0,0,1);"
    (workspaces :monitor monitor)
    (bat)
    (mem)
    (bright)
    ))

(defwidget topbar0 [monitor]
  (revealer
    :reveal show
    :transition "slidedown"
    :duration "350ms"
    (centerbox
      :class "bar_class"
      :space-evenly false
      :orientation "h"
      :style "background-color: rgba(0,0,0,0); padding-bottom: 9px; padding-top: 9px;"
      :hexpand false
        (top_left :monitor monitor)
        (window_title :monitor monitor)
        (right :monitor monitor)
    )
  )
)

(defwidget topbar1 [monitor]
  (revealer
    :reveal show
    :transition "slidedown"
    :duration "350ms"
    (box
      :class "bar_class"
      :orientation "h"
      :space-evenly false
      :style "background-color: rgba(0,0,0,0); padding-bottom: 9px; padding-top: 9px;"
      :hexpand false
      (top_left :monitor monitor)
      (center :monitor monitor)
    )
  )
)


(defwidget sep []
  (box :class "module-2" :vexpand "false" :hexpand "false" :valign "center"
      ; :style "background-color: #ffffff"
    (label :class "separ" :text "")))


(defwidget sep2 []
  (box
    (label :class "separ2" :text "|")))

(defwidget clock_module []
  (eventbox :onhover "${eww} update time_rev=true"
    :onhoverlost "${eww} update time_rev=false"
    (box :class "module" :space-evenly "false" :orientation "h" :spacing "3" :valign "center" :halign "center" 
      (label :text clock_time :class "clock_time_class" )
      (label :text "|" :class "clock_time_sep" )
      (label :text clock_minute :class "clock_minute_class")
      (revealer :transition "slideright"
        :reveal time_rev
        :duration "350ms"
        (button :class "clock_date_class"
          :onclick "$HOME/.config/eww/bar/scripts/pop calendar" clock_date
        )
      ))))

(defwidget volume []
  (eventbox :onhover "${eww} update vol_reveal=true"
    :valign "center"
    :onhoverlost "${eww} update vol_reveal=false"
    (box :space-evenly "false" :orientation "v" :valign "center"
          :style "padding-right: 6px; margin: 6px;"
            (revealer :transition "slideup"
        :reveal vol_reveal
        :duration "350ms"
        (scale    :class "volbar"
          :value volume_percent
          :orientation "v"
          :max 100
          :min 0
          :flipped true
        :onchange "pactl set-sink-volume @DEFAULT_SINK@ {}%" ))
        (label :text "墳")

    )))

(defwidget bright []
  (eventbox
        :onhover "${eww} update br_reveal=true" :onhoverlost "${eww} update br_reveal=false"
    (box
      :orientation "v"
      :style "padding-right: 6px; margin: 6px;"
      :space-evenly false
      :valign "center"
      (revealer :transition "slideup"
        :reveal br_reveal
        :valign "center"
        :tooltip "${volume_percent}%"
        :duration "350ms"
        :height 0
        (scale    :class "brightbar"
          :value brightness_percent
          :orientation "v"
          :tooltip "${brightness_percent}%"
          :max 100
          :min 0
          :flipped true
        :onchange "light -S {}%" ))
      (label :text "" :valign "center" :class "bright_icon" :tooltip "brightness")
      )

    ))
;;  Music


(defwidget bottom []
  (box :orientation "v"
    :class "bar_class"
    :style "border-radius: 16px; background-color: rgba(0,0,0,0.90)"
    :space-evenly false
    :valign "end"
    :class "left_modules"
    (workspaces :monitor 0)
    ))

(defwindow bar0
  :monitor 0
  :geometry (geometry :x "0%"
    :y "0px"
    :width "95%"
    :height "0px"
  :anchor "top center")
  :stacking "overlay"
  :windowtype "dock"
  :exclusive "true"
  (topbar0 :monitor 0))

(defwindow bar1
  :monitor 1
  :geometry (geometry :x "0%"
    :y "0px"
    :width "95%"
    :height "0px"
  :anchor "top center")
  :stacking "overlay"
  :windowtype "dock"
  :exclusive "true"  (topbar0 :monitor 1))

(defwindow bar2
  :monitor 2
  :geometry (geometry :x "0%"
    :y "0px"
    :width "95%"
    :height "0px"
  :anchor "top center")
  :stacking "overlay"
  :windowtype "dock"
  :exclusive "true"
  (topbar0 :monitor 2))

