#!/usr/bin/env nu
let $cur = (eww -c ~/.config/eww/bar get show)
if $cur =~ "true" {
  eww -c ~/.config/eww/bar update show="false";
  # eww -c ~/.config/eww/bar open bar;
} else {
  eww -c ~/.config/eww/bar update show="true";
  # eww -c ~/.config/eww/bar close bar;
}

